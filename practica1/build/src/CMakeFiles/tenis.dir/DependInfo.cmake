# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
SET(CMAKE_DEPENDS_CHECK_CXX
  "/home/barbara/sii-50678/practica1/src/Esfera.cpp" "/home/barbara/sii-50678/practica1/build/src/CMakeFiles/tenis.dir/Esfera.cpp.o"
  "/home/barbara/sii-50678/practica1/src/Mundo.cpp" "/home/barbara/sii-50678/practica1/build/src/CMakeFiles/tenis.dir/Mundo.cpp.o"
  "/home/barbara/sii-50678/practica1/src/Plano.cpp" "/home/barbara/sii-50678/practica1/build/src/CMakeFiles/tenis.dir/Plano.cpp.o"
  "/home/barbara/sii-50678/practica1/src/Raqueta.cpp" "/home/barbara/sii-50678/practica1/build/src/CMakeFiles/tenis.dir/Raqueta.cpp.o"
  "/home/barbara/sii-50678/practica1/src/Vector2D.cpp" "/home/barbara/sii-50678/practica1/build/src/CMakeFiles/tenis.dir/Vector2D.cpp.o"
  "/home/barbara/sii-50678/practica1/src/tenis.cpp" "/home/barbara/sii-50678/practica1/build/src/CMakeFiles/tenis.dir/tenis.cpp.o"
  )
SET(CMAKE_CXX_COMPILER_ID "GNU")

# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  )

# The include file search paths:
SET(CMAKE_C_TARGET_INCLUDE_PATH
  "../include"
  )
SET(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
